<?
$MYVER = getenv('APPVER');
if ((empty($MYVER)) || (!isset($MYVER))) {
    $MYVER="Unknown";
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>My Simple PHP Application</title>
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lobster+Two" type="text/css">
    <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <link rel="stylesheet" href="/styles.css" type="text/css">
</head>
<body>
    <section class="congratulations">
        <img src="myapp.png">
        <h1>Congratulations!</h1>
        <p>This is your first simple <em>PHP</em> application!&nbsp;</p>
        <p>You are running PHP version <b><?= phpversion()?></b></p>
        <p>This is <b><?echo $MYVER?></b> version of the app</p>
    </section>

    <!--[if lt IE 9]><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
</body>
</html>
